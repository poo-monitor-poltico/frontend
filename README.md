# POO Monitor Politico

Este repositório armazena todo o código frontend do projeto Monitor Politico. Para acessar os documentos entre no nosso repositório de documentação: https://gitlab.com/poo-monitor-poltico/documentation.

## Contribuir
Você quer contribuir com nosso projeto? É simples! Temos um [guia de contribuição](https://gitlab.com/poo-monitor-poltico/documentation/-/blob/main/docs/CONTRIBUTING.md) onde são explicados todos os passos para contribuir. Ahh, não esquece de ler nosso [código de conduta](https://gitlab.com/poo-monitor-poltico/documentation/-/blob/main/docs/CODE_OF_CONDUCT.md).   
Caso reste duvidas você também pode entrar em contato conosco criando uma issue.  
Para contribuir com os nossos projetos acesse nossa organização: [MonitorPolitico](https://gitlab.com/poo-monitor-poltico).

## Executando o projeto localmente com Docker
### Montando a imagem
  $ docker image build -t poo_frontend_img .

### Executando o container
  $ docker container run -it --rm -p 3000:3000 --name poo_frontend_cnt -d poo_frontend_img


