import React from 'react';
import { Route } from 'react-router-dom';
import HomePage from '../paginas/home';
import PartidosPage from '../paginas/partidos';
import CongressmenPage from '../paginas/deputados';
import CongressmenChartsPage from '../paginas/graficosDeputados';
import PartiesChartsPage from '../paginas/graficosPartidos';
import PartidoPage from '../paginas/partido';
import DeputadoPage from '../paginas/deputado';
import TopicsPage from '../paginas/topicos';
import TopicPage from '../paginas/topico';

const RotasPrivadas: React.FC = () => (
  <>
    <Route path="/home" exact>
      <HomePage />
    </Route>
    <Route path="/topico/:regex/:expressao" exact>
      <TopicPage />
    </Route>
    <Route path="/topicos" exact>
      <TopicsPage />
    </Route>
    <Route path="/graficos/partidos" exact>
      <PartiesChartsPage />
    </Route>
    <Route path="/graficos/deputados" exact>
      <CongressmenChartsPage />
    </Route>
    <Route path="/deputados" exact>
      <CongressmenPage />
    </Route>
    <Route path="/deputados/:deputadoId" exact>
      <DeputadoPage />
    </Route>
    <Route path="/partidos" exact>
      <PartidosPage />
    </Route>
    <Route path="/partidos/:partidoId" exact>
      <PartidoPage />
    </Route>
  </>
);

export default RotasPrivadas;
