import React, { useContext } from 'react';
import {
  BrowserRouter as Router,
  Switch,
} from 'react-router-dom';
import RotasPrivadas from './RotasPrivadas';
import RotasPublicas from './RotasPublicas';
import AuthContext from '../contextos/autenticacao';

const Rotas: React.FC = () => {
  const { signed } = useContext(AuthContext);

  return (
    <Router>
      <Switch>
        {signed && <RotasPrivadas />}
        <RotasPublicas />
      </Switch>
    </Router>
  );
};

export default Rotas;
