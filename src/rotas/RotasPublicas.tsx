import React from 'react';
import { Route } from 'react-router-dom';
import LoginPage from '../paginas/login';
import LogoutPage from '../paginas/logout';
import PaginaCadastro from '../paginas/cadastro';
// import NotFoundPage from '../pages/notfound';

const RotasPublicas: React.FC = () => (
  <>
    <Route path="/" exact>
      <LoginPage />
    </Route>
    <Route path="/login" exact>
      <LoginPage />
    </Route>
    <Route path="/logout" exact>
      <LogoutPage />
    </Route>
    <Route path="/cadastro" exact>
      <PaginaCadastro />
    </Route>
    {/* <Route>
      <NotFoundPage />
    </Route> */}
  </>
);

export default RotasPublicas;
