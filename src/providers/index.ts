import axios from 'axios';

export const Api = axios.create({
  baseURL: 'http://localhost:8080',
  // timeout: 30000,
  headers: {
    'Access-Control-Allow-Origin': '*',
    accept: '*/*',
    'Content-type': 'application/json',
  },
});

export default Api;
