import React from 'react';
// import logo from './logo.svg';
import './App.css';
import Rotas from './rotas';
import { AuthProvider } from './contextos/autenticacao';

const App: React.FC = () => (
  <AuthProvider>
    <Rotas />
  </AuthProvider>
);

export default App;
