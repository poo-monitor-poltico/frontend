import ServicoUsuario from './ServicoUsuario';
import ServicoProposicao from './ServicoProposicao';
import ServicoBuscaProposicao from './ServicoBuscaProposicao';
import ServicoPartido from './ServicoPartido';
import ServicoDeputado from './ServicoDeputado';
import ServicoVotos from './ServicoVotos';
import ServicoTopicos from './ServicoTopicos';

export {
  ServicoUsuario,
  ServicoProposicao,
  ServicoBuscaProposicao,
  ServicoPartido,
  ServicoDeputado,
  ServicoVotos,
  ServicoTopicos,
};
