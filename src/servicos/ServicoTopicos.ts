/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable import/prefer-default-export */
import { AxiosResponse } from 'axios';
import { ITopico } from '../interfaces';
import { Api } from '../providers';

const getAll = () => Api.get<ITopico[]>('/topicos');

const getById = (topicoId: number) => Api.get<ITopico>(`/topicos/${topicoId}`);

const adicionaTopico = (usuarioId: number, expressao: string, regex: boolean): Promise<AxiosResponse<void>> => Api.put(`/topico/adicionaTopico?idUsuario=${usuarioId}&expressao=${expressao}&regex=${regex}`);
const removeTopico = (usuarioId: number, topicoId: number): Promise<AxiosResponse<void>> => Api.put(`/topico/removeTopico?idUsuario=${usuarioId}&topicoId=${topicoId}`);

export const ServicoTopicos = {
  getAll,
  getById,
  adicionaTopico,
  removeTopico,
};

export default ServicoTopicos;
