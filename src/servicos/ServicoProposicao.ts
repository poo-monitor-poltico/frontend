import { AxiosResponse } from 'axios';
import { IProposicao } from '../interfaces';
import Api from '../providers';

const getAll = (): Promise<AxiosResponse> => Api.get<IProposicao[]>('/proposicao');

const getById = (proposicaoId: number): Promise<AxiosResponse<IProposicao>> => Api.get<IProposicao>(`/proposicao/${proposicaoId}`);

const ServicoProposicao = {
  getAll,
  getById,
};

export default ServicoProposicao;
