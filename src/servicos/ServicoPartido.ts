/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { AxiosResponse } from 'axios';
import {
  IUsuario,
  IPartido,
} from '../interfaces';
import { Api } from '../providers';

const getAll = () => Api.get<IPartido[]>('/partido');

const getById = (partidoId: string) => Api.get<IPartido>(`/partido/${partidoId}`);

const adicionaPartido = (usuarioId: number, party: IPartido): Promise<AxiosResponse<IUsuario>> => Api.put(`/partido/adicionaPartido?userid=${usuarioId}`, party);
const removePartido = (usuarioId: number, partidoId: number): Promise<AxiosResponse<IUsuario>> => Api.put(`/partido/removePartido?userid=${usuarioId}&partidoId=${partidoId}`);

const ServicoPartido = {
  getAll,
  getById,
  adicionaPartido,
  removePartido,
};

export default ServicoPartido;
