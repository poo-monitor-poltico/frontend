/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable guard-for-in */
/* eslint-disable no-restricted-syntax */
/* eslint-disable max-len */
// import { AxiosResponse } from 'axios';
import { Api } from '../providers';

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
const votosPorDeputado = (usuarioId: number, dataInicio: string, dataFim: string) => Api.get(`/voto/listaVotosPorDeputado?userid=${usuarioId}&dataInicio=${dataInicio}&dataFim=${dataFim}`);

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
const votosPorPartido = (usuarioId: number, dataInicio: string, dataFim: string) => Api.get(`/voto/listaVotosPorPartido?userid=${usuarioId}&dataInicio=${dataInicio}&dataFim=${dataFim}`);

const votosPorProposicao = (proposicaoId: number) => Api.get(`/votacao/somaTotal?id=${proposicaoId}`);

const nuvemDeputado = (deputadoId: number) => Api.get(`/voto/NuvemDeputado?deputado=${deputadoId}`);
const nuvemPartido = (partidoId: number) => Api.get(`/voto/NuvemPartido?partido=${partidoId}`);

const ServicosVotos = {
  votosPorDeputado,
  votosPorPartido,
  votosPorProposicao,
  nuvemDeputado,
  nuvemPartido,
};

export default ServicosVotos;
