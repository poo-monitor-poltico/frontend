/* eslint-disable guard-for-in */
/* eslint-disable no-restricted-syntax */
/* eslint-disable max-len */
import { AxiosResponse } from 'axios';
import {
  IUsuario,
  ICadastroUsuario,
  ITopico,
} from '../interfaces';
import { Api } from '../providers';

const login = (email: string, pw: string): Promise<AxiosResponse<boolean>> => Api.post<boolean>(`/usuario/login?email=${email}&senha=${pw}`);

const getAll = (): Promise<AxiosResponse<IUsuario[]>> => Api.get<IUsuario[]>('/usuario');
const getById = (usuarioId: number): Promise<AxiosResponse<IUsuario>> => Api.get<IUsuario>(`/usuario/${usuarioId}`);
const cadastro = (user: ICadastroUsuario): Promise<AxiosResponse<IUsuario>> => Api.post<IUsuario>('/usuario/users', user);

const getTopicosMonitorados = (usuarioId: number): Promise<AxiosResponse<ITopico[]>> => Api.get<ITopico[]>(`/usuario/${usuarioId}/topicos`);

const ServicoUsuario = {
  login,
  cadastro,
  getAll,
  getById,
  getTopicosMonitorados,
};

export default ServicoUsuario;
