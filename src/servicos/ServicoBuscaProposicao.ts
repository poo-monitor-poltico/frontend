/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable max-len */
import { AxiosResponse } from 'axios';
import Api from '../providers';

const getAll = (): Promise<AxiosResponse> => Api.get('/proposicao/Elastic/all');
const getTopicosPorExpressao = (expressao: string, regex: boolean): Promise<AxiosResponse<any>> => Api.get<any>(`/proposicao/elasticQuery?q=${encodeURIComponent(expressao)}&regex=${regex}`);

const ServicoBuscaProposicao = {
  getAll,
  getTopicosPorExpressao,
};

export default ServicoBuscaProposicao;
