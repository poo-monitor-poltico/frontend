/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable import/prefer-default-export */
import { AxiosResponse } from 'axios';
import {
  IDeputado,
  IUsuario,
  IPartido,
} from '../interfaces';
import { Api } from '../providers';

const getAll = () => Api.get<IDeputado[]>('/deputado');

const getByPartyId = (partidoId: string) => Api.get<IDeputado>(`/deputado/${partidoId}`);

const adicionaDeputado = (usuarioId: number, deputado: IPartido): Promise<AxiosResponse<IUsuario>> => Api.put(`/deputado/adicionaDeputadoMonitorado?userid=${usuarioId}`, deputado);
const removeDeputado = (usuarioId: number, deputadoId: number): Promise<AxiosResponse<IUsuario>> => Api.put(`/deputado/removeDeputadoMonitorado?userid=${usuarioId}&deputadoId=${deputadoId}`);

const ServicoDeputado = {
  getAll,
  getByPartyId,
  adicionaDeputado,
  removeDeputado,
};

export default ServicoDeputado;
