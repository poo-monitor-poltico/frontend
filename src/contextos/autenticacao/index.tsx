import React, { createContext, useState, useEffect } from 'react';
import { AxiosResponse } from 'axios';
import { Usuarios } from '../../hooks';
import { IUsuario } from '../../interfaces';

interface AuthContextData {
  signed: boolean,
  usuario: IUsuario | null,
  usuarioId: number,
  setUsuario: React.Dispatch<IUsuario | null>,
  login: (email: string, pw: string) => Promise<AxiosResponse<boolean>>,
}

const AuthContext = createContext<AuthContextData>({} as AuthContextData);

export const AuthProvider: React.FC = ({ children }) => {
  const [usuario, setUsuario] = useState<IUsuario | null>(null);
  const { loginUser } = Usuarios();

  useEffect(() => {
    const storedUserString = localStorage.getItem('userData');
    if (storedUserString) {
      const dadosUsuario = JSON.parse(storedUserString);
      setUsuario(dadosUsuario);
    }
  }, []);

  return (
    <AuthContext.Provider value={{
      signed: Boolean(usuario),
      login: loginUser,
      setUsuario,
      usuario,
      usuarioId: usuario ? usuario.id : 0,
    }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export default AuthContext;
