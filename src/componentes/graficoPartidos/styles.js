import styled from 'styled-components';

export const PieChartWrapper = styled.div`
  > canvas {
    width: 20vw !important;
    position: relative;
    margin: 40px auto 40px auto;
  }

`;
export const BarChartWrapper = styled.div`
  > canvas {
    width: 60vw !important;
    position: relative;
    margin: 40px auto 40px auto;
  }

`;
