import styled from 'styled-components';

const ChartWrapper = styled.div`
  > canvas {
    width: 20vw !important;
    position: relative;
    margin: auto;
  }

`;

export default ChartWrapper;
