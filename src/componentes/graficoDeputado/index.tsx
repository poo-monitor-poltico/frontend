/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable max-len */
/* eslint-disable guard-for-in */
/* eslint-disable no-restricted-syntax */
import React, { useState, useEffect } from 'react';
import { Pie } from 'react-chartjs-2';
import {
  IPartido,
  IDeputado,
  IProposicao,
  IVotoProposicao,
} from '../../interfaces';
import {
  ServicoDeputado,
  ServicoPartido,
  ServicoProposicao,
  ServicoVotos,
} from '../../servicos';
import 'chart.js';
import ChartWrapper from './styles.js';

type InputProps = {
  usuarioId: number,
  dataInicio: string,
  dataFim: string,
  deputadoId: number
}

const Graficos: React.FC<InputProps> = ({
  usuarioId, dataInicio, dataFim, deputadoId,
}) => {
  const [partidos, setPartidos] = useState<IPartido[]>([]);
  const [proposicoes, setProposicoes] = useState<IProposicao[]>([]);
  const [deputados, setDeputados] = useState<IDeputado[]>([]);
  const [votos, setVotos] = useState<IVotoProposicao[]>([]);
  const [votosPorTipo, setVotosPorTipo] = useState<number[]>([0, 0, 0, 0, 0]);
  const [dadosTabela, setDadosTabela] = useState<any[]>([]);
  const [data, setData] = useState<any>(null);
  const [error, setError] = useState<string>('');

  useEffect(() => {
    ServicoDeputado.getAll().then(({ status, data }) => {
      if (status === 200) {
        setDeputados(data);
      }
    });

    ServicoPartido.getAll().then(({ status, data }) => {
      if (status === 200) {
        setPartidos(data);
      }
    });

    ServicoProposicao.getAll().then(({ status, data }) => {
      if (status === 200) {
        setProposicoes(data);
      }
    });
  }, []);

  useEffect(() => {
    setError('');
    const getVotosPorDeputado = (usuarioId: number, dataInicio: string, dataFim: string) => {
      ServicoVotos.votosPorDeputado(usuarioId, dataInicio, dataFim)
        .then(({ status, data }) => {
          const temp: IVotoProposicao[] = [];
          if (status === 200) {
            for (const proposicaoId in data) {
              data[proposicaoId].forEach((arr: any[]) => {
                arr.forEach((proposicao: any) => {
                  temp.push({
                    ...proposicao,
                    proposicaoId,
                  });
                });
              });
            }
            setVotos(temp);
          } else {
            setVotos([]);
            setDadosTabela([]);
            setError('Selecione um intervalo de datas válido!');
          }
          return temp;
        })
        .catch((err) => {
          setVotos([]);
          setDadosTabela([]);
          // eslint-disable-next-line no-console
          console.error(err);
          setError('Selecione um intervalo de datas válido!');

          return null;
        });
    };
    getVotosPorDeputado(usuarioId, dataInicio, dataFim);
  }, [dataInicio, dataFim]);

  useEffect(() => {
    if (votos && votos.length) {
      const newCounter = [0, 0, 0, 0, 0];
      const votosDeputados = votos.map((v) => {
        if (v.parlamentarId === deputadoId) {
          switch (v.opcao) {
            case 'SIM':
              newCounter[0] += 1;
              break;
            case 'NAO':
              newCounter[1] += 1;
              break;
            case 'ABSTENCAO':
              newCounter[2] += 1;
              break;
            case 'OBSTRUCAO':
              newCounter[3] += 1;
              break;
            case 'AUSENTE':
              newCounter[4] += 1;
              break;
            default:
          }
        }
        return ({
          ...v,
          deputado: deputados.filter((c) => c.id === v.parlamentarId)[0],
        }
        );
      });
      const votosDeputadoProposicao = votosDeputados.map((v) => ({
        ...v,
        proposicao: proposicoes.filter((p) => p.id === Number(v.proposicaoId))[0],
      }));

      const votosDeputadoProposicaoFiltrado = votosDeputadoProposicao.filter((v) => v.parlamentarId === deputadoId);
      setVotosPorTipo(newCounter);
      setDadosTabela(votosDeputadoProposicaoFiltrado);
    } else {
      setVotosPorTipo([0, 0, 0, 0, 0]);
    }
  }, [deputados, partidos, proposicoes, votos]);

  useEffect(() => {
    const newData = {
      labels: ['SIM', 'NAO', 'ABSTENCAO', 'OBSTRUCAO', 'AUSENTE'],
      datasets: [
        {
          label: 'Distribuição do votos do deputado no período',
          data: votosPorTipo,
          backgroundColor: [
            'rgba(54, 235, 162, 0.2)',
            'rgba(255, 99, 132, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)',
          ],
          borderColor: [
            'rgba(54, 235, 162, 1)',
            'rgba(255, 99, 132, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
          ],
          borderWidth: 1,
        },
      ],
    };
    setData(newData);
  }, [votosPorTipo]);

  const options = {
    responsive: true,
    mantainAspectRatio: true,
  };

  const PieChart = () => (
    <>
      <h2 className="title is-size-5 has-text-centered">Distribuição dos votos do deputado no período</h2>
      <Pie data={data} options={options} redraw />
    </>
  );

  return (
    <>
      {
        (data && !error)
          ? (
            <ChartWrapper>
              <PieChart />
            </ChartWrapper>
          ) : <p className="subtitle is-6 has-text-centered has-text-danger">{error}</p>
      }

      <div className="table-container mx-6" style={{ maxHeight: '500px', overflow: 'scroll' }}>
        <table className="table is-fullwidth is-striped">
          <thead>
            <tr>
              <th>#</th>
              <th>Sigla da proposição</th>
              <th>Número da proposta</th>
              <th>Voto</th>
              <th>Ano da proposição</th>
              <th>Autor principal</th>
              <th>Data de apresentação</th>
              <th>Descrição</th>
              <th>Indexação</th>
            </tr>
          </thead>
          <tbody>
            {dadosTabela && dadosTabela.map((v, i) => (
              v.proposicao
              && (
                <tr key={v.id}>
                  <td>{i + 1}</td>
                  <td>{v.proposicao.sigla}</td>
                  <td>{Number(v.proposicao.numero)}</td>
                  <td>{v.opcao}</td>
                  <td>{v.proposicao.ano}</td>
                  <td>{v.proposicao.autor_principal}</td>
                  <td>{v.proposicao.data_apresentacao}</td>
                  <td>{v.proposicao.descricao}</td>
                  <td>{v.proposicao.indexacao}</td>
                </tr>
              )
            ))}
          </tbody>
        </table>
      </div>
    </>
  );
};

export default Graficos;
