/* eslint-disable @typescript-eslint/no-explicit-any */
import React from 'react';
import GraficoProposicao from '../graficoProposicao';

type InputProps = {
  proposicao: any,
}

const Proposicao: React.FC<InputProps> = ({ proposicao }) => (
  <div className="mb-4">
    <h3><strong>{`${proposicao.sigla} ${proposicao.numero} (${proposicao.ano})`}</strong></h3>
    <p><strong>Ementa:</strong> {proposicao.ementa}</p>
    <p><strong>Descrição:</strong> {proposicao.descricao}</p>
    <p><strong>Indexação:</strong> {proposicao.indexacao}</p>
    <GraficoProposicao proposicaoId={proposicao.id} />
  </div>
);

export default Proposicao;
