/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable guard-for-in */
/* eslint-disable no-restricted-syntax */
import React, { useEffect, useState } from 'react';
import { TagCloud } from 'react-tagcloud';
import { ServicoVotos } from '../../servicos';

type InputProps = {
  deputadoId: string,
}

const NuvemPalavras: React.FC<InputProps> = ({ deputadoId }) => {
  const [dadosSim, setDadosSim] = useState<any>(null);
  const [dadosNao, setDadosNao] = useState<any>(null);

  useEffect(() => {
    ServicoVotos.nuvemDeputado(Number(deputadoId)).then(({ status, data }) => {
      if (status === 200) {
        const tmpSim = [];
        const tmpNao = [];

        for (const key in data.first) {
          tmpSim.push({ value: key, count: data.first[key] });
        }
        for (const key in data.second) {
          tmpNao.push({ value: key, count: data.second[key] });
        }
        setDadosSim(tmpSim);
        setDadosNao(tmpNao);
      }
    });
  }, []);

  return (
    <>
      <div className="my-6">

        <h2 className="title is-size-5 has-text-centered">Nuvem de Palavras (Sim)</h2>
        {
          dadosSim
          && (
            <TagCloud
              minSize={12}
              maxSize={35}
              tags={dadosSim}
            // onClick={(tag: any) => alert(`'${tag.value}' was selected!`)}
            />
          )
        }
      </div>
      <div className="my-6">
        <h2 className="title is-size-5 has-text-centered">Nuvem de Palavras (Não)</h2>
        {dadosNao
          && (
            <TagCloud
              minSize={12}
              maxSize={35}
              tags={dadosNao}
            // onClick={(tag: any) => alert(`'${tag.value}' was selected!`)}
            />
          )}
      </div>
    </>
  );
};

export default NuvemPalavras;
