import React from 'react';
import * as S from './styles.js';

const Container: React.FC = ({ children }) => (
  <S.Wrapper>
    {children}
  </S.Wrapper>
);

export default Container;
