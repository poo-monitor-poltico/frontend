/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useEffect, useState } from 'react';
import { Pie } from 'react-chartjs-2';
import { ServicoVotos } from '../../servicos';
import ChartWrapper from './styles.js';

type InputProps = {
  proposicaoId: number
}

const GraficoProposicao: React.FC<InputProps> = ({ proposicaoId }) => {
  const [votos, setVotos] = useState<number[]>([0, 0, 0]);
  const [dadosGrafico, setDadosGrafico] = useState<any>(null);
  const [erro, setErro] = useState<string>('');

  useEffect(() => {
    ServicoVotos.votosPorProposicao(proposicaoId)
      .then(({ status, data }) => {
        if (status === 200 && data) {
          setVotos(data);
        } else {
          setErro('data is null');
        }
      })
      .catch((error) => {
        // eslint-disable-next-line no-console
        console.error(error);
        setErro('error');
      });
  }, []);

  useEffect(() => {
    if (votos) {
      const newData = {
        labels: ['Sim', 'Não', 'Outros'],
        datasets: [
          {
            label: 'Distribuição do votos do deputado no período',
            data: votos,
            backgroundColor: [
              'rgba(54, 235, 162, 0.2)',
              'rgba(255, 99, 132, 0.2)',
              'rgba(255, 206, 86, 0.2)',
            ],
            borderColor: [
              'rgba(54, 235, 162, 1)',
              'rgba(255, 99, 132, 1)',
              'rgba(255, 206, 86, 1)',
            ],
            borderWidth: 1,
          },
        ],
      };
      setDadosGrafico(newData);
    }
  }, [votos]);

  const options = {
    responsive: true,
    mantainAspectRatio: true,
  };

  const PieChart = () => (
    <Pie data={dadosGrafico} options={options} redraw />
  );

  return (
    <>
      {
        dadosGrafico && !erro ? (
          <>
            {
              !erro && (
                <ChartWrapper>
                  <PieChart />
                </ChartWrapper>
              )
            }
          </>
        ) : (
          // <p>Erro no gráfico</p>
          <></>
        )

      }
    </>
  );
};

export default GraficoProposicao;
