import React, { useState, useEffect } from 'react';
import TextField from '@mui/material/TextField';
import { IDeputado } from '../../interfaces';

type InputProps = {
  lista: IDeputado[],
  setListaDeBusca: React.Dispatch<React.SetStateAction<IDeputado[]>>,
  label: string,
}

const Busca: React.FC<InputProps> = ({ lista, setListaDeBusca, label }: InputProps) => {
  const [termoBuscado, setTermoBuscado] = useState('');

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setTermoBuscado(e.target.value);
  };

  useEffect(() => {
    const novaLista = lista.filter((p) => p.nome.toLowerCase().includes(termoBuscado.toLowerCase()));
    setListaDeBusca(novaLista);
  }, [termoBuscado]);

  return (
    <div className="center cm-10">
      <TextField
        id="outlined-basic"
        label={label}
        variant="outlined"
        size="small"
        value={termoBuscado}
        onChange={handleChange}
      />
    </div>
  );
};

export default Busca;
