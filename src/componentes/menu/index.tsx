/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import * as S from './styles.js';
import LogoLight from '../../assets/images/logo_light.svg';
import 'bulma';

const Menu: React.FC = () => (
  <S.Nav className="navbar" role="navigation" aria-label="main navigation">
    <div className="navbar-brand">
      <a className="navbar-item" href="/home">
        <S.Title>
          <img className="navbar-image" src={LogoLight} alt="logo" />
        </S.Title>
      </a>

      <a role="button" className="navbar-burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
        <span aria-hidden="true" />
        <span aria-hidden="true" />
        <span aria-hidden="true" />
      </a>
    </div>

    <div id="navbarBasicExample" className="navbar-menu">
      <S.NavbarItems>
        <a className="navbar-item" href="/home">
          Home
        </a>
        <a className="navbar-item" href="/partidos">
          Partidos
        </a>
        <a className="navbar-item" href="/deputados">
          Deputados
        </a>
        <a className="navbar-item" href="/topicos">
          Tópicos
        </a>
      </S.NavbarItems>
      <div className="navbar-end">
        <S.NavbarUserIcon>
          <div className="navbar-item">
            <div className="navbar-item has-dropdown is-hoverable">
              <a className="navbar-link">
                <S.UserIconWrapper>
                  <AccountCircleIcon fontSize="large" />
                </S.UserIconWrapper>
              </a>
              <div className="navbar-dropdown">
                <a className="navbar-item" href="/logout">
                  Logout
                </a>
              </div>
            </div>
          </div>
        </S.NavbarUserIcon>
      </div>
    </div>
  </S.Nav>
);

export default Menu;
