import styled from 'styled-components';

export const Nav = styled.nav`
  background-color: #6C63FF;

  * {
    color: #fff;
  }
  
  .navbar-brand {
    display: flex;
    justify-content: center;
    align-items: center;
  }
  .navbar-dropdown {
    background-color: #6C63FF !important;
  }
  .navbar-link {
    background-color: #5561cd !important;
    border-color: yellow;
  }
  .navbar-link::after {
    background-color: #5561cd !important;
    border-color: yellow;
  }
`;

export const Title = styled.span`
  color: #fff;
  font-weight: bold;
  display: flex;
  justify-content: center;
  align-items: center;

  .navbar-image {
    max-height: 60px;
  }
`;

export const UserIconWrapper = styled.span`
  background-color: #6C63FF !important;
  display: flex;
  justify-content: center;
  align-items: center;

  svg {
    fill: white;
  }
`;

export const NavbarItems = styled.div`
  display: flex;

  * {
    color: #fff;
  }

  .navbar-item:hover{
    color: #6C63FF !important;
  }
`;

export const NavbarUserIcon = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  .navbar-link {
    background-color: #6C63FF !important;
  }

  .navbar-link::after {
    background-color: #6C63FF !important;
    border-color: white;
  }
`;
