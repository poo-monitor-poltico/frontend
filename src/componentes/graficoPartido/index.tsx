/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable no-restricted-syntax */
/* eslint-disable max-len */
/* eslint-disable guard-for-in */
import React, { useState, useEffect } from 'react';
import { Pie } from 'react-chartjs-2';
import {
  IPartido,
  IDeputado,
  IProposicao,
  IVotoProposicao,
} from '../../interfaces';
import {
  ServicoDeputado,
  ServicoPartido,
  ServicoProposicao,
  ServicoVotos,
} from '../../servicos';
import 'chart.js';
import * as S from './styles.js';

type InputProps = {
  usuarioId: number,
  dataInicio: string,
  dataFim: string,
  partidoId: string,
}

const Graficos: React.FC<InputProps> = ({
  usuarioId, dataInicio, dataFim, partidoId,
}) => {
  const [partidos, setPartidos] = useState<IPartido[]>([]);
  const [proposicoes, setProposicoes] = useState<IProposicao[]>([]);
  const [deputados, setDeputados] = useState<IDeputado[]>([]);
  const [votos, setVotos] = useState<any[]>([]);
  const [data, setData] = useState<any[]>([]);
  const [error, setError] = useState<string>('');

  const [contador, setContador] = useState<number[]>([]);

  const [dadosPizza, setDadosPizza] = useState<any>(null);

  useEffect(() => {
    ServicoDeputado.getAll().then(({ status, data }) => {
      if (status === 200) {
        setDeputados(data);
      }
    });

    ServicoPartido.getAll().then(({ status, data }) => {
      if (status === 200) {
        setPartidos(data);
      }
    });

    ServicoProposicao.getAll().then(({ status, data }) => {
      if (status === 200) {
        setProposicoes(data);
      }
    });
  }, []);

  useEffect(() => {
    // Abre a estrutura do endpoint de votos
    const getVoteData = (usuarioId: number, dataInicio: string, dataFim: string) => {
      ServicoVotos.votosPorPartido(usuarioId, dataInicio, dataFim)
        .then(({ status, data }) => {
          const temp: IVotoProposicao[] = [];
          if (status === 200) {
            for (const proposicaoId in data) {
              data[proposicaoId].forEach((arr: any[]) => {
                arr.forEach((proposicao: any) => {
                  temp.push({
                    ...proposicao,
                    proposicaoId,
                  });
                });
              });
            }
            setVotos(temp);
          } else {
            setVotos([]);
          }
          return temp;
        })
        .catch((err) => {
          setVotos([]);
          // eslint-disable-next-line no-console
          console.error(err);
          return null;
        });
    };
    getVoteData(usuarioId, dataInicio, dataFim);
  }, [dataInicio, dataFim]);

  // Joins
  useEffect(() => {
    setError('');
    if (partidos.length && proposicoes.length && deputados.length && partidos.length && votos.length) {
      const votosDeputados = votos.map((vote) => ({
        ...vote,
        deputado: deputados.filter((deputados) => deputados.id === vote.parlamentarId)[0],
      }));
      const votosDeputadosParty = votosDeputados.map((vote) => ({
        ...vote,
        partido: partidos.filter((partido) => partido.id === vote.deputado.partido_id)[0],
      }));
      const votosDeputadoProposicao = votosDeputadosParty.map((vote) => ({
        ...vote,
        proposicao: proposicoes.filter((p) => p.id === Number(vote.proposicaoId))[0],
      }));
      const filteredJoinedVotes = votosDeputadoProposicao.filter((vote) => vote.partido.id === Number(partidoId));
      setData(filteredJoinedVotes);
    } else {
      setData([]);
      setError('Selecione um intervalo de datas válido!');
    }
  }, [partidos, proposicoes, deputados, votos]);

  // Getting charts data
  useEffect(() => {
    if (data.length) {
      const contadorPizza = [0, 0, 0, 0, 0];

      // Pie chart
      data.forEach((vote) => {
        switch (vote.opcao) {
          case 'SIM':
            contadorPizza[0] += 1;
            break;
          case 'NAO':
            contadorPizza[1] += 1;
            break;
          case 'ABSTENCAO':
            contadorPizza[2] += 1;
            break;
          case 'OBSTRUCAO':
            contadorPizza[3] += 1;
            break;
          case 'AUSENTE':
            contadorPizza[4] += 1;
            break;
          default:
        }
      });

      setContador(contadorPizza);
    } else {
      setContador([]);
    }
  }, [data]);

  useEffect(() => {
    const newData = {
      labels: ['SIM', 'NAO', 'ABSTENCAO', 'OBSTRUCAO', 'AUSENTE'],
      datasets: [
        {
          label: 'Distribuição do votos do deputado no período',
          data: contador,
          backgroundColor: [
            'rgba(54, 235, 162, 0.2)',
            'rgba(255, 99, 132, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)',
          ],
          borderColor: [
            'rgba(54, 235, 162, 1)',
            'rgba(255, 99, 132, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
          ],
          borderWidth: 1,
        },
      ],
    };
    setDadosPizza(newData);
  }, [contador]);

  const optionsPie = {
    responsive: true,
    mantainAspectRatio: true,
  };

  const PieChart = () => (
    <>
      <h2 className="title is-size-5 has-text-centered">Distribuição dos votos totais no período</h2>
      <Pie data={dadosPizza} options={optionsPie} redraw />
    </>
  );

  return (
    <>
      {
        (dadosPizza && !error)
          ? (
            <S.PieChartWrapper>
              <PieChart />
            </S.PieChartWrapper>
          ) : <p className="subtitle is-6 has-text-centered has-text-danger">{error}</p>
      }
      <div className="table-container mx-6" style={{ maxHeight: '500px', overflow: 'scroll' }}>
        <table className="table is-fullwidth is-striped">
          <thead>
            <tr>
              <th>#</th>
              <th>Deputado</th>
              <th>Sigla da proposição</th>
              <th>Número da proposta</th>
              <th>Voto</th>
              <th>Ano da proposição</th>
              <th>Autor principal</th>
              <th>Data de apresentação</th>
              <th>Descrição</th>
              <th>Indexação</th>
            </tr>
          </thead>
          <tbody>
            {data && data.map((vote, index) => (
              <tr key={vote.id}>
                <td>{index + 1}</td>
                <td>{vote.deputado.nome}</td>
                <td>{vote.proposicao.sigla}</td>
                <td>{Number(vote.proposicao.numero)}</td>
                <td>{vote.opcao}</td>
                <td>{vote.proposicao.ano}</td>
                <td>{vote.proposicao.autor_principal}</td>
                <td>{vote.proposicao.data_apresentacao}</td>
                <td>{vote.proposicao.descricao}</td>
                <td>{vote.proposicao.indexacao}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </>
  );
};

export default Graficos;
