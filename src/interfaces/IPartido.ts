export interface IPartido {
  id: number,
  nome: string,
  numero: number,
  cor: string,
  monitorado?: boolean,
}
