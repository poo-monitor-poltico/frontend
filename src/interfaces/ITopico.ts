/* eslint-disable camelcase */
export interface ITopico {
  id: number,
  expressao_chave: string,
  regex: boolean,
}
