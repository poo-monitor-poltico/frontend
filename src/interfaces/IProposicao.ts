/* eslint-disable camelcase */
export interface IVotoProposicao {
  id: string,
  proposicaoId: number,
  parlamentarId: number,
  votacaoId: string,
  opcao: string,
}

export interface IProposicao {
  id: number,
  id_prop: string,
  descricao: string,
  sigla: string,
  numero: string,
  ano: string,
  indexacao: string,
  autor_principal: string,
  data_apresentacao: string | null,
  casa_legislativa_id: number,
}
