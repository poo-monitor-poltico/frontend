/* eslint-disable camelcase */

export interface IDeputado {
  id: number,
  parlamentar_id: number,
  nome: string,
  genero: string,
  localidade: string,
  partido_id: number,
  casaLegislativaId: number,
  monitorado?: boolean,
}
