export * from './IUsuario';
export * from './IPartido';
export * from './IDeputado';
export * from './IProposicao';
export * from './ITopico';
