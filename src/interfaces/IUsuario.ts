import { IPartido, IDeputado, ITopico } from '.';

export interface ICadastroUsuario {
  email: string,
  senha: string,
  partidosMonitorados: IPartido[],
  deputadosMonitorados: IDeputado[],
}

export interface IUsuario {
  id: number,
  email: string,
  senha: string,
  partidosMonitorados: IPartido[],
  deputadosMonitorados: IDeputado[],
  topicosMonitorados: ITopico[],
}
