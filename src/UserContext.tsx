import { createContext } from 'react';

interface LoggedUser {
  name: string | null
}

const UserContext = createContext<LoggedUser>({
  name: null,
});

export default UserContext;
