/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable import/prefer-default-export */
import { useState, useCallback } from 'react';
import { IUsuario, ICadastroUsuario } from '../interfaces';
import { ServicoUsuario } from '../servicos';

export const Usuarios = () => {
  const [usuarios, setUsuarios] = useState<IUsuario[]>([]);
  const [logged, setLogged] = useState<boolean>(false);

  const getAllUsers = useCallback(async () => {
    const { status, data } = await ServicoUsuario.getAll();
    if (status === 200) setUsuarios(data);
  }, []);

  const loginUser = useCallback(async (email: string, pw: string) => {
    const resp = await ServicoUsuario.login(email, pw);
    return resp;
  }, []);

  const cadastroUsuario = useCallback(async (user: ICadastroUsuario) => {
    const { status, data } = await ServicoUsuario.cadastro(user);
    if (status === 200 && data) return data;
    return null;
  }, []);

  const getContadorMonitorados = useCallback(async (usuarioId: number) => {
    const { status, data } = await ServicoUsuario.getById(usuarioId);
    if (status === 200 && data) {
      return {
        deputados: data.deputadosMonitorados.length,
        partidos: data.partidosMonitorados.length,
        topicos: data.topicosMonitorados.length,
      };
    }
    return null;
  }, []);

  return {
    loginUser,
    cadastroUsuario,
    getAllUsers,
    logged,
    usuarios,
    getContadorMonitorados,
  };
};
