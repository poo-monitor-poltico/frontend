/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { useState, useCallback } from 'react';
import { IPartido } from '../interfaces';
import { ServicoPartido } from '../servicos';

const Partidos = () => {
  const [partidos, setPartidos] = useState<IPartido[]>([]);

  const getAllParties = useCallback(async () => {
    const { status, data } = await ServicoPartido.getAll();
    if (status === 200) setPartidos(data);
  }, []);

  return {
    getAllParties,
    partidos,
  };
};

export default Partidos;
