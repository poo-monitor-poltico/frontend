import React from 'react';

const PaginaNaoEncontrada: React.FC = () => (
  <div>
    <h1>Página não encontrada</h1>
  </div>
);

export default PaginaNaoEncontrada;
