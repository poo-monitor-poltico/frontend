/* eslint-disable import/prefer-default-export */
import styled from 'styled-components';
import image from '../../assets/images/contextualizacao.png';

export const RegisterContainer = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  background-color: #D0CFDD;
  
`;

export const ImageBackground = styled.div`
  width: 70%;
  height: 100vh;

  background-image: url(${image});
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;

  @media(max-width: 800px) {
    display: none;
  }
`;

export const FormContainer = styled.div`
  width: 30%;
  min-width: 360px;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;

  @media(max-width: 800px) {
    width: 100%;
  }
`;

export const Form = styled.form`
  display: flex;
  flex-direction: column;
  width: 360px;
  padding: 0px 20px;

  * {
    margin-bottom: 40px;
  }
`;
