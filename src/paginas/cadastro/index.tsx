import React, { useState } from 'react';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Paper from '@mui/material/Paper';
import { useHistory } from 'react-router-dom';
import * as S from './styles.js';
import Container from '../../componentes/container';

import { Usuarios } from '../../hooks';
// import { IUsuario } from '../../interfaces/index.js';

const PaginaCadastro: React.FC = () => {
  const [nome, setNome] = useState<string>('');
  const [email, setEmail] = useState<string>('');
  const [senha, setSenha] = useState<string>('');
  const { cadastroUsuario } = Usuarios();
  const history = useHistory();

  const handleRegister = () => {
    const body = {
      nome,
      email,
      senha,
      partidosMonitorados: [],
      deputadosMonitorados: [],
    };
    cadastroUsuario(body)
      .then(() => {
        history.push('/login');
      });
  };

  const handleChangeNome = (e: React.ChangeEvent<HTMLInputElement>) => {
    setNome(e.target.value);
  };

  const handleChangeEmail = (e: React.ChangeEvent<HTMLInputElement>) => {
    setEmail(e.target.value);
  };

  const handleChangeSenha = (e: React.ChangeEvent<HTMLInputElement>) => {
    setSenha(e.target.value);
  };

  return (
    <Container>
      <S.RegisterContainer>
        <S.ImageBackground />
        <S.FormContainer>
          <Paper elevation={3}>
            <S.Form>
              <h1 className="title has-text-centered">Cadastro</h1>
              <TextField id="outlined-basic" label="Nome" variant="outlined" name="nome" onChange={handleChangeNome} value={nome} />
              <TextField id="outlined-basic" label="Email" variant="outlined" name="email" onChange={handleChangeEmail} value={email} />
              <TextField id="outlined-basic" label="Senha" variant="outlined" name="pw" onChange={handleChangeSenha} value={senha} type="password" />
              <Button variant="contained" onClick={handleRegister}>Cadastrar</Button>
            </S.Form>
          </Paper>
        </S.FormContainer>
      </S.RegisterContainer>
    </Container>
  );
};

export default PaginaCadastro;
