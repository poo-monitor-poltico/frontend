import styled from 'styled-components';

export const SearchWrapper = styled.div`
  background-color: #f2f2f2;
  margin: 40px;
  border-radius: 20px;
  padding: 20px;
`;

export const TableContainer = styled.div`
  margin: 20px;
`;
