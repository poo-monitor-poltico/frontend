/* eslint-disable max-len */
/* eslint-disable guard-for-in */
/* eslint-disable no-restricted-syntax */
import React, { useState, useEffect, useContext } from 'react';
import { useParams } from 'react-router';
import Paper from '@mui/material/Paper';
import Menu from '../../componentes/menu';
import Charts from '../../componentes/graficoDeputado';
import { IDeputado } from '../../interfaces';
import { ServicoDeputado } from '../../servicos';
import AuthContext from '../../contextos/autenticacao';
import NuvemPalavras from '../../componentes/nuvemPalavras';
import * as S from './styles.js';

interface Params {
  deputadoId: string,
}

const PaginaDeputado: React.FC = () => {
  const [deputado, setCongressman] = useState<IDeputado>();
  const [dataInicio, setDataInicio] = useState<string>('2012-08-01');
  const [dataFim, setDataFim] = useState<string>('2015-08-22');

  const { usuario } = useContext(AuthContext);
  const { deputadoId } = useParams<Params>();

  useEffect(() => {
    ServicoDeputado.getAll().then(({ status, data }) => {
      if (status === 200) {
        const resp = data.filter((d) => d.id === Number(deputadoId))[0];
        setCongressman(resp);
      }
    });
  }, []);

  const handleInitialDateChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
    const { value } = e.target;
    setDataInicio(value);
  };

  const handleFinalDateChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
    setDataFim(e.target.value);
  };

  return (
    <>
      <Menu />
      <div className="container my-6">
        <h1 className="title mt-3">
          Deputado: {deputado?.nome}
        </h1>
        <div className="is-flex is-justify-content-center mb-5">
          <label htmlFor="start">Data inicial
            <input
              className="mx-3"
              type="date"
              name="start"
              id="start"
              value={dataInicio}
              onChange={handleInitialDateChange}
              max={dataFim || '2021-12-01'}
              style={{ borderRadius: '5px' }}

            />
          </label>
          <label htmlFor="start">Data final
            <input
              className="mx-3 p-1"
              type="date"
              name="final"
              id="final"
              value={dataFim}
              onChange={handleFinalDateChange}
              min={dataInicio || '2021-10-01'}
              style={{ borderRadius: '5px' }}
            />
          </label>
        </div>
        <S.TableContainer>
          <Paper>
            {usuario
            && (
              <Charts
                usuarioId={usuario.id}
                dataInicio={dataInicio}
                dataFim={dataFim}
                deputadoId={Number(deputadoId)}
              />
            )}
          </Paper>
        </S.TableContainer>
      </div>

      <div className="has-text-centered">
        <NuvemPalavras deputadoId={deputadoId} />
      </div>

    </>
  );
};

export default PaginaDeputado;
