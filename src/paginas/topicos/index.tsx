import React, { useContext, useEffect, useState } from 'react';
import Paper from '@mui/material/Paper';
import { Link, useHistory } from 'react-router-dom';

import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import Menu from '../../componentes/menu';
import { ITopico } from '../../interfaces';
import {
  ServicoUsuario,
  ServicoTopicos,
} from '../../servicos';
import AuthContext from '../../contextos/autenticacao';
import * as S from './styles.js';

const CongressmenPage: React.FC = () => {
  const [topico, setTopico] = useState<string>('');
  const [topicos, setTopicos] = useState<ITopico[]>([]);
  const [tipo, setTipo] = useState<string>('normal');
  const { usuario } = useContext(AuthContext);
  const history = useHistory();

  useEffect(() => {
    const usuarioId = usuario ? usuario.id : 0;
    if (usuarioId) {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      ServicoUsuario.getTopicosMonitorados(usuarioId).then(({ status, data }) => {
        setTopicos(data);
      });
    }
  }, [usuario]);

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setTopico(e.target.value);
  };

  const handleChangeRadio = (e: React.ChangeEvent<HTMLInputElement>) => {
    setTipo(e.target.value);
  };

  const handleFollow = () => {
    if (usuario) {
      const regex = tipo === 'regex';
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      ServicoTopicos.adicionaTopico(usuario.id, encodeURIComponent(topico), regex).then(({ status, data }) => {
        history.push('/');
        history.goBack();
      });
    }
  };

  const handleRemove = (usuarioId: number | undefined, topicId: number) => {
    if (usuario) {
      ServicoTopicos.removeTopico(usuario?.id, topicId).then(() => {
        history.push('/');
        history.goBack();
      });
    }
  };

  return (
    <>
      <Menu />
      <S.TopicosContainer>
        <S.MainContainer>
          <S.SearchSelectContainer>
            <Paper elevation={3}>
              <h1 className="title has-text-centered mt-3">Tópicos</h1>
              <h2 className="has-text-centered">Insira uma palavra ou expressão que gostaria de acompanhar</h2>
              <div className="is-flex is-justify-content-center is-align-content-center my-6">
                <S.SearchContainer>
                  <TextField
                    id="outlined-basic"
                    label="Tópico"
                    variant="outlined"
                    name="topic"
                    onChange={handleChange}
                    value={topico}
                  />
                  <button type="button" className="button is-primary is-medium mx-4" onClick={handleFollow}>Acompanhar tópico</button>
                  <div style={{ marginLeft: '34px' }}>
                    <RadioGroup
                      aria-label="exptype"
                      defaultValue="regex"
                      name="radio-buttons-group"
                      value={tipo}
                      onChange={handleChangeRadio}
                    >
                      <FormControlLabel value="normal" control={<Radio />} label="Expressão normal" />
                      <FormControlLabel value="regex" control={<Radio />} label="Expressão regular" />
                    </RadioGroup>
                  </div>
                </S.SearchContainer>
              </div>
            </Paper>
          </S.SearchSelectContainer>
          <S.SearchSelectContainer>
            <Paper elevation={3}>
              <h1 className="title has-text-centered">Tópicos Monitorados</h1>
              <div className="is-flex is-justify-content-center">
                <S.TableContainer>
                  <div className="table-container" style={{ overflow: 'scroll' }}>
                    <table className="table is-fullwidth is-striped">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Expressão</th>
                          <th>Tipo</th>
                        </tr>
                      </thead>
                      <tbody>
                        {topicos && topicos.map((topico, index) => (
                          <tr key={topico.id}>
                            <td>{index}</td>
                            <td>{topico.expressao_chave}</td>
                            <td>{topico.regex ? 'Expressão regular' : 'Normal'}</td>
                            <td>
                              <button
                                type="button"
                                className="button is-primary is-danger is-small"
                                onClick={() => handleRemove(usuario?.id, topico.id)}
                              >
                                Remover tópico
                              </button>
                            </td>
                            <td>
                              <Link to={`/topico/${topico.regex}/${topico.expressao_chave}`}>
                                <button type="button" className="button is-info is-small">Ver detalhes</button>
                              </Link>
                            </td>
                          </tr>
                        ))}
                      </tbody>
                    </table>
                  </div>
                </S.TableContainer>
              </div>
            </Paper>
          </S.SearchSelectContainer>
        </S.MainContainer>
      </S.TopicosContainer>
    </>
  );
};

export default CongressmenPage;
