import React, { useState, useContext } from 'react';
import Menu from '../../componentes/menu';
import Charts from '../../componentes/graficoPartidos';
import AuthContext from '../../contextos/autenticacao';

const PaginaGraficosPartidos: React.FC = () => {
  const [dataInicio, setDataInicio] = useState<string>('2012-08-01');
  const [dataFim, setDataFim] = useState<string>('2012-08-22');

  const { usuario } = useContext(AuthContext);

  const handleInitialDateChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
    const { value } = e.target;
    setDataInicio(value);
  };

  const handleFinalDateChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
    setDataFim(e.target.value);
  };

  return (
    <>
      <Menu />
      <div className="container my-6">
        <h1 className="title mt-3">
          Gráficos de Partidos
        </h1>
        <div className="is-flex is-justify-content-center mb-5">
          <label htmlFor="start">Data inicial
            <input
              className="mx-3"
              type="date"
              name="start"
              id="start"
              value={dataInicio}
              onChange={handleInitialDateChange}
              max={dataFim || '2021-12-01'}
              style={{ borderRadius: '5px' }}

            />
          </label>
          <label htmlFor="start">Data final
            <input
              className="mx-3 p-1"
              type="date"
              name="final"
              id="final"
              value={dataFim}
              onChange={handleFinalDateChange}
              min={dataInicio || '2021-10-01'}
              style={{ borderRadius: '5px' }}
            />
          </label>
        </div>

        {usuario
          && (
            <Charts
              usuarioId={usuario.id}
              dataInicio={dataInicio}
              dataFim={dataFim}
            />
          )}
      </div>

    </>
  );
};

export default PaginaGraficosPartidos;
