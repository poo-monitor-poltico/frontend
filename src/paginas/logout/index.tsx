import { useEffect, useContext } from 'react';
import { useHistory } from 'react-router-dom';
import AuthContext from '../../contextos/autenticacao';

const PaginaLogout: (() => null) = () => {
  const auth = useContext(AuthContext);
  const history = useHistory();

  useEffect(() => {
    auth.setUsuario(null);
    localStorage.clear();
    history.push('/login');
  }, []);

  return null;
};

export default PaginaLogout;
