import React, { useContext, useEffect, useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import Paper from '@mui/material/Paper';
import Menu from '../../componentes/menu';
import * as S from './styles.js';
import { IPartido } from '../../interfaces';
import { ServicoPartido, ServicoUsuario } from '../../servicos';
import Search from '../../componentes/buscaPartido';
import AuthContext from '../../contextos/autenticacao';

const PaginaPartidos: React.FC = () => {
  const [partidos, setPartidos] = useState<IPartido[]>([]);
  const [partidosMonitorados, setPartidosMonitorados] = useState<IPartido[]>([]);
  const [listaDeBusca, setListaDeBusca] = useState<IPartido[]>([]);

  const { usuario } = useContext(AuthContext);
  const history = useHistory();

  useEffect(() => {
    ServicoPartido.getAll().then(({ status, data }) => {
      if (status === 200) {
        setPartidos(data);
        setListaDeBusca(data);
      }
    });
  }, []);

  useEffect(() => {
    if (usuario) {
      ServicoUsuario.getById(usuario.id).then(({ status, data }) => {
        if (status === 200) {
          setPartidosMonitorados(data.partidosMonitorados);
        }
      });
    }
  }, [usuario]);

  const handleFollowParty = (usuarioId: number | undefined, partidoId: number) => {
    const partido = {
      id: partidoId,
      nome: 'nome',
      numero: 0,
      cor: 'cor',
    };
    if (usuario) {
      ServicoPartido.adicionaPartido(usuario?.id, partido)
        .then(() => {
          history.push('/');
          history.goBack();
        })
        .catch(((error) => {
          // eslint-disable-next-line no-console
          console.error(error);
        }));
    }
  };

  const handleRemovePartido = (usuarioId: number | undefined, partidoId: number) => {
    if (usuario) {
      ServicoPartido.removePartido(usuario?.id, partidoId).then(() => {
        history.push('/');
        history.goBack();
      });
    }
  };

  return (
    <>
      <Menu />
      <S.PartidoContainer>
        <S.MainContainer>
          <S.SearchSelectContainer>
            <Paper elevation={3}>
              <h2 className="title has-text-centered">Lista de Partidos</h2>
              <Search
                lista={partidos}
                setListaDeBusca={setListaDeBusca}
                label="Partido"
              />
              <S.TableContainer>
                <div className="table-container" style={{ maxHeight: '300px', overflow: 'scroll' }}>
                  <table className="table is-fullwidth is-striped">
                    <thead>
                      <tr>
                        <th>Sigla</th>
                        <th>Número</th>
                      </tr>
                    </thead>
                    <tbody>
                      {listaDeBusca.map((p) => (
                        <tr key={p.id}>
                          <td>{p.nome}</td>
                          <td>{p.numero}</td>
                          <td>
                            <button
                              type="button"
                              className="button is-primary is-light is-small"
                              onClick={() => handleFollowParty(usuario?.id, p.id)}
                            >
                              Monitorar
                            </button>
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </div>
              </S.TableContainer>
            </Paper>
          </S.SearchSelectContainer>
          <S.SearchSelectContainer>
            <Paper elevation={3}>
              <h2 className="title has-text-centered">Partidos monitorados</h2>
              {
                partidosMonitorados.length === 0
                && <p className="subtitle is-6 has-text-centered has-text-danger">Você ainda não monitora nenhum partido</p>
              }
              {
                partidosMonitorados.length > 0
                && (
                  <S.TableContainer>
                    <div className="table-container" style={{ maxHeight: '300px', overflow: 'scroll' }}>
                      <table className="table is-fullwidth is-striped">
                        <thead>
                          <tr>
                            <th>Nome</th>
                            <th>Número</th>
                            <th>Cor</th>
                          </tr>
                        </thead>
                        <tbody>
                          {partidosMonitorados.map((p) => (
                            <tr key={p.id}>
                              <td>{p.nome}</td>
                              <td>{p.numero}</td>
                              <td style={{ backgroundColor: p.cor }} />
                              <td>
                                <button
                                  type="button"
                                  className="button is-primary is-danger is-small"
                                  onClick={() => handleRemovePartido(usuario?.id, p.id)}
                                >
                                  Remover partido
                                </button>
                              </td>
                              <td>
                                <Link to={`/partidos/${p.id}`}>
                                  <button type="button" className="button is-primary is-info is-small">Ver detalhes</button>
                                </Link>
                              </td>
                            </tr>
                          ))}
                        </tbody>
                      </table>
                    </div>
                  </S.TableContainer>
                )
              }
            </Paper>
          </S.SearchSelectContainer>
        </S.MainContainer>
        <S.BodyContainer>
          <div className="has-text-centered my-6">
            <Link to="/graficos/partidos">
              <button type="button" className="button is-primary is-light is-large">Acompanhar</button>
            </Link>
          </div>
        </S.BodyContainer>
      </S.PartidoContainer>
    </>
  );
};

export default PaginaPartidos;
