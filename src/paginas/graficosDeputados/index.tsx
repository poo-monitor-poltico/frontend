/* eslint-disable max-len */
/* eslint-disable guard-for-in */
/* eslint-disable no-restricted-syntax */
import React, { useState, useEffect, useContext } from 'react';
import { useParams } from 'react-router';
import Menu from '../../componentes/menu';
import Charts from '../../componentes/graficoDeputados';
import { IDeputado } from '../../interfaces';
import { ServicoDeputado } from '../../servicos';
import AuthContext from '../../contextos/autenticacao';

interface Params {
  deputadoId: string,
}

const PaginaGraficosDeputado: React.FC = () => {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [deputado, setCongressman] = useState<IDeputado>();
  const [dataInicio, setDataInicio] = useState<string>('2012-08-01');
  const [dataFim, setDataFim] = useState<string>('2015-08-22');

  const { deputadoId } = useParams<Params>();
  const { usuario } = useContext(AuthContext);

  useEffect(() => {
    ServicoDeputado.getAll().then(({ status, data }) => {
      if (status === 200) {
        const resp = data.filter((d) => d.id === Number(deputadoId))[0];
        setCongressman(resp);
      }
    });
  }, []);

  const handleInitialDateChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
    const { value } = e.target;
    setDataInicio(value);
  };

  const handleFinalDateChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
    setDataFim(e.target.value);
  };

  return (
    <>
      <Menu />
      <div className="container my-6">
        <h1 className="title mt-3">
          Gráficos de Deputados
        </h1>
        <div className="is-flex is-justify-content-center mb-5">
          <label htmlFor="start">Data inicial
            <input
              className="mx-3"
              type="date"
              name="start"
              id="start"
              value={dataInicio}
              onChange={handleInitialDateChange}
              max={dataFim || '2021-12-01'}
              style={{ borderRadius: '5px' }}

            />
          </label>
          <label htmlFor="start">Data final
            <input
              className="mx-3 p-1"
              type="date"
              name="final"
              id="final"
              value={dataFim}
              onChange={handleFinalDateChange}
              min={dataInicio || '2021-10-01'}
              style={{ borderRadius: '5px' }}
            />
          </label>
        </div>

        {usuario
          && (
            <Charts
              usuarioId={usuario.id}
              dataInicio={dataInicio}
              dataFim={dataFim}
            />
          )}
      </div>

    </>
  );
};

export default PaginaGraficosDeputado;
