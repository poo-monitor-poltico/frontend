/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useState, useEffect, useContext } from 'react';
import Paper from '@mui/material/Paper';
import FlagIcon from '@mui/icons-material/Flag';
import PersonIcon from '@mui/icons-material/Person';
import BookIcon from '@mui/icons-material/Book';
import Menu from '../../componentes/menu';
import * as S from './styles.js';
import AuthContext from '../../contextos/autenticacao';
import { Usuarios } from '../../hooks';

type Contador = {
  deputados: number,
  partidos: number,
  topicos: number,
}

const PaginaHome: React.FC = () => {
  const [contador, setContador] = useState<Contador | null>(null);

  const { usuarioId } = useContext(AuthContext);
  const { getContadorMonitorados } = Usuarios();

  useEffect(() => {
    getContadorMonitorados(usuarioId)
      .then((data) => {
        setContador(data);
      });
  }, []);

  const cardFields = [
    {
      name: 'Partidos',
      quantity: contador ? contador.partidos : 0,
      icon: <FlagIcon />,
    },
    {
      name: 'Deputados',
      quantity: contador ? contador.deputados : 0,
      icon: <PersonIcon />,
    },
    {
      name: 'Tópicos',
      quantity: contador ? contador.topicos : 0,
      icon: <BookIcon />,
    },
  ];

  const CardComponent = (name: string, number: number, icon: any) => (
    <S.CardWrap>
      <Paper elevation={3}>
        <S.CardContent>
          <h2>{name}</h2>
          <p>{number}</p>
          <S.CardIcon className="icon">
            {icon}
          </S.CardIcon>
        </S.CardContent>
      </Paper>
    </S.CardWrap>
  );

  return (
    <div>
      <Menu />
      <S.HomeContainer>
        <S.MainContainer>
          <S.CardsContainer>
            {cardFields.map((field) => CardComponent(field.name, field.quantity, field.icon))}
          </S.CardsContainer>
        </S.MainContainer>
        <S.BodyComponent>
          <Paper elevation={3}>
            <S.ChartComponent>
              GRAFICO 1
            </S.ChartComponent>
          </Paper>
          <Paper elevation={3}>
            <S.ChartComponent>
              GRAFICO 2
            </S.ChartComponent>
          </Paper>
        </S.BodyComponent>
      </S.HomeContainer>
    </div>
  );
};

export default PaginaHome;
