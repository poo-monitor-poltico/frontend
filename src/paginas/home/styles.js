/* eslint-disable import/prefer-default-export */
import styled from 'styled-components';

export const HomeContainer = styled.div`
  width: 100%;
  height: 100vh;
`;

export const MainContainer = styled.div`
  display: flex;
  flex-direction: column;
`;

export const CardsContainer = styled.div`
  width: 100%;
  background-color: #6C63FF;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  padding: 30px 10%;
`;

export const CardContent = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 15px 20px;
  max-width: 400px;
  min-width: 250px;

  h2 {
    font-size: 18px !important;
  }

  p {
    color: #838383;
    font-weight: thin;
    font-size: 16px !important;
  }
`;

export const CardWrap = styled.div`
  .MuiPaper-rounded {
    border-radius: 10px !important;
  }

  :hover {

    h2 {
      color: white;
    }
    
    p {
      color: white;
    }

    .MuiPaper-rounded {
      background-color: #2F2E41;
    }
  
    .icon {
      background-color: white;
    }
  
    svg {
      color: #2F2E41;
    }
  }

`;

export const CardIcon = styled.div`
  width: 40px;
  height: 40px;
  border-radius: 20px;
  background-color: #2F2E41;

  display: flex;
  justify-content: center;
  align-items: center;

  svg {
    color: white;
  }
`;

export const BodyComponent = styled.div`
  justify-content: space-between;
  align-items: center;
  padding: 15px 20px;
  display: flex;
  flex-direction: row;
  width: 100%;
`;

export const ChartComponent = styled.div`
  display: flex;
  justify-content: center;
  max-width: 670px;
  min-width: 200px;
  width: 650px;
  height: 500px;
  border-radius: 20px;
`;
