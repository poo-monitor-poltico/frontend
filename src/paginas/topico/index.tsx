/* eslint-disable no-debugger */
import React, { useEffect, useState } from 'react';
import Paper from '@mui/material/Paper';
import { useParams } from 'react-router';
import { ServicoBuscaProposicao } from '../../servicos';
import Menu from '../../componentes/menu';
import Proposicao from '../../componentes/proposicao';
import * as S from './styles.js';

interface Params {
  expressao: string,
  regex: string,
}

const PaginaTopico: React.FC = () => {
  const [proposicoes, setProposicoes] = useState([]);

  const { expressao, regex } = useParams<Params>();

  useEffect(() => {
    ServicoBuscaProposicao.getTopicosPorExpressao(expressao, regex === 'true').then(({ status, data }) => {
      console.log(regex);
      if (status === 200) {
        setProposicoes(data);
      }
    });
  }, []);

  return (
    <>
      <Menu />
      <div className="container">
        {
          proposicoes && proposicoes.length ? (
            <>
              <h2 className="title has-text-centered mt-6">Propostas encontradas</h2>
              <Paper elevation={3}>
                <S.ListContainer>
                  {proposicoes.length && proposicoes.map((proposicao) => (<Proposicao proposicao={proposicao} />))}
                </S.ListContainer>
              </Paper>
            </>
          )
            : <h2 className="title has-text-centered mt-6">Nenhuma proposta encontrada</h2>

        }
      </div>
    </>
  );
};

export default PaginaTopico;
