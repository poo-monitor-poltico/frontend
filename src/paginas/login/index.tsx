import React, { useState, useEffect, useContext } from 'react';
import { Link, useHistory } from 'react-router-dom';
import Button from '@mui/material/Button';
import Paper from '@mui/material/Paper';
import TextField from '@mui/material/TextField';
import * as S from './styles.js';
import Container from '../../componentes/container';
import AuthContext from '../../contextos/autenticacao';
import { IUsuario } from '../../interfaces';
import { ServicoUsuario } from '../../servicos';

const PaginaLogin: React.FC = () => {
  const auth = useContext(AuthContext);
  const history = useHistory();

  const [email, setEmail] = useState<string>('');
  const [senha, setSenha] = useState<string>('');
  const [allUsers, setAllUsers] = useState<IUsuario[]>([]);
  const [error, setError] = useState<string>('');

  useEffect(() => {
    localStorage.clear();
    ServicoUsuario.getAll().then((r) => {
      if (r.status === 200) {
        setAllUsers(r.data);
      }
    });
  }, []);

  const handleChangeEmail = (e: React.ChangeEvent<HTMLInputElement>) => {
    setEmail(e.target.value);
  };

  const handleChangeSenha = (e: React.ChangeEvent<HTMLInputElement>) => {
    setSenha(e.target.value);
  };

  const userDataToLocalStorage = () => {
    const userData = allUsers.filter((u) => u.email === email)[0];
    localStorage.setItem('userData', JSON.stringify(userData));
    auth.setUsuario(userData);
    history.push('/partidos');
  };

  const handleLogin = () => {
    auth.login(email, senha)
      .then((r) => {
        if (r.status === 200 && r.data) {
          return userDataToLocalStorage();
        }
        return setError('Usuário ou senha inválidos.');
      })
      .catch(() => setError('Usuário ou senha inválidos.'));
  };

  return (
    <Container>
      <S.LoginContainer>
        <S.ImageBackground />
        <S.FormContainer>
          <Paper elevation={3}>
            <S.Form>
              <h1 className="title has-text-centered">Faça seu login</h1>
              <TextField
                id="outlined-basic"
                label="Email"
                variant="outlined"
                name="email"
                onChange={handleChangeEmail}
                value={email}
              />
              <TextField
                id="outlined-basic"
                label="Senha"
                variant="outlined"
                name="senha"
                onChange={handleChangeSenha}
                value={senha}
                type="password"
              />
              {error && <p className="has-text-danger">{error}</p>}
              <Button variant="contained" onClick={handleLogin}>Entrar</Button>
            </S.Form>
            <div style={{ margin: '20px 0' }}>
              <Link to="/cadastro">
                <span style={{ margin: '0 20px' }}>Cadastrar</span>
              </Link>
            </div>
          </Paper>
        </S.FormContainer>
      </S.LoginContainer>
    </Container>
  );
};

export default PaginaLogin;
