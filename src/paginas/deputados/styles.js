import styled from 'styled-components';

export const DeputadosContainer = styled.div`
  display: flex;
  width: 100%;
  height: 100vh;
  flex-direction: column;
`;

export const MainContainer = styled.div`
  justify-content: space-between;
  padding: 15px 10px;
  display: flex;
  flex-direction: row;
  width: 100%;
`;

export const BodyContainer = styled.div`
  
`;

export const SearchSelectContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  max-width: 800px;
  min-width: 200px;
  width: 100%;
  height: 500px;
  border-radius: 20px;
`;

export const TableContainer = styled.div`
  width: 600px;
  margin: 20px;
`;

export const SearchWrapper = styled.div`
  background-color: #f2f2f2;
  margin: 40px;
  border-radius: 20px;
  padding: 20px;
`;
