import React, { useContext, useEffect, useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import Paper from '@mui/material/Paper';
import Menu from '../../componentes/menu';
import * as S from './styles.js';
import { IDeputado } from '../../interfaces';
import { ServicoDeputado, ServicoUsuario } from '../../servicos';
import BuscaDeputado from '../../componentes/buscaDeputado';
import AuthContext from '../../contextos/autenticacao';

const PaginaDeputados: React.FC = () => {
  const [deputados, setDeputados] = useState<IDeputado[]>([]);
  const [followedCongressmen, setFollowedCongressmen] = useState<IDeputado[]>([]);
  const [listaDeBusca, setListaDeBusca] = useState<IDeputado[]>([]);

  const { usuario } = useContext(AuthContext);
  const history = useHistory();

  useEffect(() => {
    ServicoDeputado.getAll().then(({ status, data }) => {
      if (status === 200) {
        setDeputados(data);
        setListaDeBusca(data);
      }
    });
  }, []);

  useEffect(() => {
    if (usuario) {
      ServicoUsuario.getById(usuario.id).then(({ status, data }) => {
        if (status === 200) {
          setFollowedCongressmen(data.deputadosMonitorados);
        }
      });
    }
  }, [usuario]);

  const handleFollowCongressman = (usuarioId: number | undefined, partidoId: number) => {
    const deputado = {
      id: partidoId,
      nome: 'nome',
      numero: 0,
      cor: 'cor',
    };
    if (usuario) {
      ServicoDeputado.adicionaDeputado(usuario?.id, deputado).then(() => {
        history.push('/');
        history.goBack();
      });
    }
  };

  const handleRemoveCongressman = (usuarioId: number | undefined, deputadoId: number) => {
    if (usuario) {
      ServicoDeputado.removeDeputado(usuario?.id, deputadoId).then(() => {
        history.push('/');
        history.goBack();
      });
    }
  };

  return (
    <>
      <Menu />
      <S.DeputadosContainer>
        <S.MainContainer>
          <S.SearchSelectContainer>
            <Paper elevation={3}>
              <h2 className="title has-text-centered">Lista de Deputados</h2>
              <BuscaDeputado
                lista={deputados}
                setListaDeBusca={setListaDeBusca}
                label="Deputado"
              />
              <S.TableContainer>
                <div className="table-container" style={{ maxHeight: '300px', overflow: 'scroll' }}>
                  <table className="table is-fullwidth is-striped">
                    <thead>
                      <tr>
                        <th>Nome</th>
                        <th>Id do Partido</th>
                        <th>Id da Casa Legislativa</th>
                      </tr>
                    </thead>
                    <tbody>
                      {listaDeBusca.map((p) => (
                        <tr key={p.id}>
                          <td>{p.nome}</td>
                          <td>{p.partido_id}</td>
                          <td>{p.casaLegislativaId}</td>
                          <td>
                            <button
                              type="button"
                              className="button is-primary is-light is-small"
                              onClick={() => handleFollowCongressman(usuario?.id, p.id)}
                            >
                              Monitorar
                            </button>
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </div>
              </S.TableContainer>
            </Paper>
          </S.SearchSelectContainer>
          <S.SearchSelectContainer>
            <Paper elevation={3}>
              <h2 className="title has-text-centered">Deputados monitorados</h2>
              {
                followedCongressmen.length === 0
                && <p className="subtitle is-6 has-text-centered has-text-danger">Você ainda não monitora nenhum deputado</p>
              }
              {
                followedCongressmen.length > 0
                && (
                  <S.TableContainer>
                    <div className="table-container" style={{ maxHeight: '300px', overflow: 'scroll' }}>
                      <table className="table is-fullwidth is-striped">
                        <thead>
                          <tr>
                            <th>Nome</th>
                            <th>Id do Partido</th>
                            <th>Id da Casa Legislativa</th>
                          </tr>
                        </thead>
                        <tbody>
                          {followedCongressmen.map((p) => (
                            <tr key={p.id}>
                              <td>{p.nome}</td>
                              <td>{p.partido_id}</td>
                              <td>{p.casaLegislativaId}</td>
                              <td>
                                <button
                                  type="button"
                                  className="button is-primary is-danger is-small"
                                  onClick={() => handleRemoveCongressman(usuario?.id, p.id)}
                                >
                                  Remover deputado
                                </button>
                              </td>
                              <td>
                                <Link to={`/deputados/${p.id}`}>
                                  <button type="button" className="button is-info is-small">Ver detalhes</button>
                                </Link>
                              </td>
                            </tr>
                          ))}
                        </tbody>
                      </table>
                    </div>
                  </S.TableContainer>
                )
              }
            </Paper>
          </S.SearchSelectContainer>
        </S.MainContainer>
        <div className="has-text-centered my-6">
          <Link to="/graficos/deputados">
            <button type="button" className="button is-primary is-light is-large">Acompanhar</button>
          </Link>
        </div>
      </S.DeputadosContainer>
    </>
  );
};

export default PaginaDeputados;
