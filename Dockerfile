FROM node:16.11.1-alpine3.12

RUN mkdir /app
WORKDIR /app

COPY package.json package.json
RUN npm install

COPY . .

CMD ["npm", "start"]